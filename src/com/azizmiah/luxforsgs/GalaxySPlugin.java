package com.azizmiah.luxforsgs;

import java.math.BigDecimal;

import com.vitocassisi.lux.plugin.PassiveDisplay;

public class GalaxySPlugin extends PassiveDisplay {

	public String[] canCauseSysIssues() {

		return null;

	}

	public PassiveDisplay.LuxBundle getBacklightLevel() {

		return PassiveDisplay.LuxBundle.builder(
				"/sys/class/backlight/s5p_bl/brightness", DELIMITED_NONE, 0,
				255);
	}

	public PassiveDisplay.LuxBundle getButtonLightLevel() {

		return null;

	}

	public PassiveDisplay.LuxBundle getLuxValue() {

		return null;

	}

	public PassiveDisplay.LuxBundle getMaxBacklight() {

		return PassiveDisplay.LuxBundle.builder(
				"/sys/class/backlight/s5p_bl/max_brightness", DELIMITED_NONE,
				0, 255);
	}

	public PassiveDisplay.LuxBundle getRGB() {
		// NOTE: Not used by Lux yet, can safely ignore for now
		return PassiveDisplay.LuxBundle.builder(
				"/sys/class/misc/colorcontrol/multiplier", " ", 0, 255);
	}

	public boolean isSupportedDevice(String paramString1, String paramString2) {
		return true;
	}

	public String[] onCleanup() {

		String[] arrayOfString1 = super.onCleanup();
		String[] arrayOfString2 = new String[1 + arrayOfString1.length];
		System.arraycopy(arrayOfString1, 0, arrayOfString2, 0,
				arrayOfString1.length);
		arrayOfString2[arrayOfString1.length] = "chmod 666 /sys/class/backlight/s5p_bl/brightness";
		return arrayOfString2;

	}

	public String[] setBacklightLevel(int paramInt) {

		String[] arrayOfString = new String[2];
		arrayOfString[0] = "chmod 666 /sys/class/backlight/s5p_bl/brightness";
		arrayOfString[1] = ("echo \"" + paramInt + "\" > /sys/class/backlight/s5p_bl/brightness");
		return arrayOfString;

	}

	public String[] setButtonLightLevel(int paramInt) {

		return null;

	}

	public String[] setMaxBacklight(int paramInt) {

		String[] arrayOfString = new String[2];
		arrayOfString[0] = "chmod 666 /sys/class/backlight/s5p_bl/max_brightness";
		arrayOfString[1] = ("echo \"" + Math.round(255.0F) + "\" > /sys/class/backlight/s5p_bl/max_brightness");
		return arrayOfString;

	}

	public String[] setRGB(int paramInt1, int paramInt2, int paramInt3) {

		String[] arrayOfString = new String[6];

		arrayOfString[0] = "chmod 666 /sys/class/misc/color_tuning/red_multiplier";
		arrayOfString[1] = "chmod 666 /sys/class/misc/color_tuning/green_multiplier";
		arrayOfString[2] = "chmod 666 /sys/class/misc/color_tuning/blue_multiplier";


		// Red multiplier
		double calculation1 = (16854000D * (double) paramInt1) - 2807000D;
		
		calculation1 = (calculation1 < 0) ? 2 : calculation1;
		calculation1 = (calculation1 > 4294967294L) ? 4294967294L : calculation1;
		
		String fileValue1 = new BigDecimal(calculation1).toPlainString();
		arrayOfString[3] = ("echo \"" + (fileValue1) + "\" > /sys/class/misc/color_tuning/red_multiplier");
		

		// Green multiplier
		double calculation2 = (16854000D * (double) paramInt2) - 2807000D;
		
		calculation2 = (calculation2 < 0) ? 2 : calculation2;
		calculation2 = (calculation2 > 4294967294L) ? 4294967294L : calculation2;
		
		String fileValue2 = new BigDecimal(calculation2).toPlainString();
		arrayOfString[4] = ("echo \"" + (fileValue2) + "\" > /sys/class/misc/color_tuning/green_multiplier");

		
		//  Blue multiplier
		double calculation3 = (16854000D * (double) paramInt3) - 2807000D;
		
		calculation3 = (calculation3 < 0) ? 2 : calculation3;
		calculation3 = (calculation3 > 4294967294L) ? 4294967294L : calculation3;
		
		String fileValue3 = new BigDecimal(calculation3).toPlainString();
		arrayOfString[5] = ("echo \"" + (fileValue3) + "\" > /sys/class/misc/color_tuning/blue_multiplier");

		
		return arrayOfString;
	}

}
