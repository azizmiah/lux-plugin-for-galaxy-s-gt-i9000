#  RGB multiplier files
## ADB pull & check
`adb pull /sys/class/misc/color_tuning/red_multiplier c:/test   && type c:\test\red_multiplier`

`adb pull /sys/class/misc/color_tuning/green_multiplier c:/test && type c:\test\green_multiplier`

`adb pull /sys/class/misc/color_tuning/blue_multiplier c:/test  && type c:\test\blue_multiplier`


## ADB push & restore
`echo 4294967294>c:/test/red_multiplier   &&  adb push c:/test/red_multiplier /sys/class/misc/color_tuning/`

`echo 4294967294>c:/test/green_multiplier &&  adb push c:/test/green_multiplier /sys/class/misc/color_tuning/`

`echo 4294967294>c:/test/blue_multiplier  &&  adb push c:/test/blue_multiplier /sys/class/misc/color_tuning/`

---

#  Backlight 
## ADB pull & check 
`adb pull /sys/class/backlight/s5p_bl/brightness c:/test && type c:\test\brightness`
