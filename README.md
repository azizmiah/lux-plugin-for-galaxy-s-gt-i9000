# Lux plugin for Samsung Galaxy S (GT-I9000)

---

## Warning!
Please be aware that Lux for Galaxy S is a **work in progress** and may have serious bugs so please handle with care. I will not be held responsible if all of a sudden your screen turns dim and becomes barely usable. 

*(Although that bug should now be fixed, if it were to happen, rebooting your phone fixes that.)*

---

## What is this?
This is an *addon* for Lux for Galaxy S (i9000) users - to use this, you'll need to make sure you have Lux installed first.

You can get Lux over at the [Google Play Store](https://play.google.com/store/apps/details?id=com.vito.lux).

## What does it do?
Using the description given to the [Nexus 4](https://play.google.com/store/apps/details?id=com.vitocassisi.lux.plugin.nexus4) and [Galaxy Nexus](https://play.google.com/store/apps/details?id=com.vitocassisi.lux.plugin.galaxynexus) plugins:

> What does this plug-in provide?
>
> * Higher quality astronomer/night modes and subzero adjustment.
> * On screen buttons are adjusted
> * Screenshots are no longer discoloured
> * Improved contrast

## I've read the warning and I still want to try it. How can I get this for my SGS?
Just head over to the [downloads page](https://bitbucket.org/azizmiah/lux-plugin-for-galaxy-s-gt-i9000/downloads) and choose the latest apk.

## Instructions & important information
This plug-in can **only be used on rooted Galaxy S (i9000) devices** using Lux Auto Brightness v1.3 or higher! 

You must enable plug-in support under the Advanced section of Lux preferences for it to work.

Should you decide to uninstall the plugin, it's strongly recommended that you reboot your device.

---

## How does it work?
The Galaxy S plugin utilises the Lux Plugin API to interface with device-specific system files to manipulate brightness/RGB levels. More information on the Lux Plug-in Library can be found [here](https://bitbucket.org/VitoCassisi/lux-plugin-library/overview).

If you want a more detailed look, have a look at the source for this plugin and/or the example plugin over on the Lux Plug-in Library repo.

## RGB multiplier formulae calcuations
To set the RGB multiplier values correctly using RGB value parameters (0-255), a formula has to be used to translate a value of 0-255 to a multiplier value. This is achieved using `y = 16854000x - 2807000`. If you have an idea for a better, more accurate formula, let me know. You may also find the screen brightness worksheet of use (available in /data/ under Source).

---

## Thanks
This plugin wouldn't have been possible if it were not for the help of Lux developer, Vito Cassisi, who took his time to help a PHP developer turned Java newbie everytime he hit a snag in development.